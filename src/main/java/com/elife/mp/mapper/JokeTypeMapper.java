package com.elife.mp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elife.mp.pojo.Joke;
import com.elife.mp.pojo.JokeType;
import org.springframework.stereotype.Repository;

@Repository
public interface JokeTypeMapper extends BaseMapper<JokeType> {
}
