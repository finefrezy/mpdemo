package com.elife.mp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.elife.mp.pojo.Joke;
import com.elife.mp.vo.JokeInfoVO;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JokeMapper extends BaseMapper<Joke> {

    // 注解方式实现多表查询
    @Select("select j.id,j.joke_name,j.joke_content,j.joke_type,j.create_time,jt.type_name" +
            " from joke j,joke_type jt where j.joke_type=#{jokeType} and j.joke_type=jt.id")
    List<JokeInfoVO> selectJokeInfo(Integer jokeType);


    List<JokeInfoVO> selectJokeInfoXml(Integer jokeType);
}
