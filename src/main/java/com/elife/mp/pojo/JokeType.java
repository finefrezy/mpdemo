package com.elife.mp.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@TableName(value = "joke_type")
public class JokeType {
    @TableId(value="id",type = IdType.AUTO)
    private Integer id;
    private String typeName;
    private Date createTime;
    private Date updateTime;
}
