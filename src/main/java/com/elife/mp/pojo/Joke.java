package com.elife.mp.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
// 自动生成get/set方法
@Getter
@Setter
@TableName(value="joke")
public class Joke {
    @TableId(value="id",type = IdType.AUTO)
    private Long id;
    private String jokeName;
    private String jokeContent;
    private Integer jokeType;
    private Date createTime;
    private Date updateTime;
}
