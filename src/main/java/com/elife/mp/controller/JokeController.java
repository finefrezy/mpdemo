package com.elife.mp.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.elife.mp.pojo.Joke;
import com.elife.mp.service.JokeService;
import com.elife.mp.vo.JokeInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@RestController
@RequestMapping("joke")
public class JokeController {

    @Autowired
    JokeService jokeService;

    @RequestMapping("insert")
    public String insertJoke(){
        Joke joke = new Joke();
        joke.setJokeContent("aaa");
        joke.setJokeType(1);
        joke.setJokeName("bbb");
        int value = jokeService.insertJoke(joke);
        return value > 0 ? "success" : "fail";
    }
    @RequestMapping("update")
    public String updateJoke(){
        Joke joke = new Joke();
        joke.setJokeContent("ddd");
        joke.setJokeType(1);
        joke.setId(13L);
        joke.setJokeName("ddd");
        joke.setUpdateTime(new Date());
        int value = jokeService.updateJokeById(joke);
        return value > 0 ? "success" : "fail";
    }


    @RequestMapping("delete")
    public String deleteJoke(){
        int value = jokeService.deleteById(13);
        return value > 0 ? "success" : "fail";
    }

    @RequestMapping("selectid")
    public Joke selectById(){
        return jokeService.selectById(1);
    }

    @RequestMapping("selectmap")
    public List<Joke> selectByMap(){
        Map<String, Object> map = new HashMap<String, Object>();
        // 写的是数据表中的列名，而非实体类的属性名
        map.put("joke_type", 1);
        map.put("joke_name","快递");
        return jokeService.selectByMap(map);
    }

    @RequestMapping("selectids")
    public List<Joke> selectByIds(){
        List<Integer> list =  new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        return jokeService.selectByIdList(list);
    }


    @RequestMapping("selectwrapper")
    public List<Joke> selectWrapper(){
        Joke joke = new Joke();
        joke.setJokeType(1);
        return jokeService.selectWrapper(joke);
    }

    @RequestMapping("selectByPage")
    public IPage<Joke> selectByPage(){
        // 参数都可以前端传入
        Joke joke = new Joke();
        joke.setJokeType(1);
        int currPage = 1;
        int pageSize = 3;
        return jokeService.selectByPage(joke,currPage,pageSize);
    }


    @RequestMapping("selectJokeInfo")
    List<JokeInfoVO> selectJokeInfo(){
        return jokeService.selectJokeInfo(1);
    }


    @RequestMapping("selectJokeInfoXml")
    List<JokeInfoVO> selectJokeInfoXml(){
        return jokeService.selectJokeInfoXml(2);
    }
}
