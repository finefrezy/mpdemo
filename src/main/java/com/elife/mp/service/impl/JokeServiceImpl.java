package com.elife.mp.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.elife.mp.mapper.JokeMapper;
import com.elife.mp.pojo.Joke;
import com.elife.mp.service.JokeService;
import com.elife.mp.vo.JokeInfoVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class JokeServiceImpl implements JokeService {

    @Autowired
    JokeMapper jokeMapper;
    @Override
    public List<Joke> selectAllJoke() {
        return null;
    }

    @Override
    public int insertJoke(Joke joke) {
        return jokeMapper.insert(joke);
    }

    @Override
    public int updateJokeById(Joke joke) {
        // 没有传值的字段不会进行更新
        return jokeMapper.updateById(joke);
    }

    @Override
    public int deleteById(Integer id) {
        //  根据map删除
        // jokeMapper.deleteBatchIds(map);
        // 根据id集合批量删除
        // jokeMapper.deleteBatchIds(list);
        // 根据id删除
        return jokeMapper.deleteById(id);
    }

    @Override
    public Joke selectById(Integer id) {
        return jokeMapper.selectById(id);
    }

    @Override
    public List<Joke> selectByMap(Map map) {
        return jokeMapper.selectByMap(map);
    }

    @Override
    public List<Joke> selectByIdList(List<Integer> ids) {
        return jokeMapper.selectBatchIds(ids);
    }



    public List<Joke> selectWrapper(Joke joke){
        // 根据条件查询所有列
        // SELECT id,joke_content,joke_name,create_time,joke_type,update_time FROM joke WHERE (joke_type = ?)
        //List<Joke> jokes = jokeMapper.selectList(new QueryWrapper<Joke>().eq("joke_type",joke.getJokeType()));

        // 根据条件查询指定列
        // SELECT joke_name,joke_content FROM joke WHERE (joke_type = ?)
        // List<Joke> jokes = jokeMapper.selectList(new QueryWrapper<Joke>().select("joke_name","joke_content").eq("joke_type",joke.getJokeType()));


        List<Joke> jokes = jokeMapper.selectList(
                new QueryWrapper<Joke>()
                        .select("joke_name","joke_content")
                        //.eq("joke_type",joke.getJokeType())
                        .like("joke_content","好")
                // 根据更多条件查询可以继续通过点去尝试
        );

        return jokes;
    }

    @Override
    public IPage selectByPage(Joke joke, int currPage, int pageSize) {
        //SQL 执行如下
        //  SELECT COUNT(1) FROM joke WHERE (joke_type = ?)
        //  SELECT id,joke_content,joke_name,create_time,joke_type,update_time FROM joke WHERE (joke_type = ?) LIMIT ?,?
        return jokeMapper.selectPage(new Page<Joke>(currPage,pageSize),
                new QueryWrapper<Joke>().eq("joke_type",1));
    }

    @Override
    public List<JokeInfoVO> selectJokeInfo(Integer jokeType) {
        return jokeMapper.selectJokeInfo(jokeType);
    }

    public List<JokeInfoVO> selectJokeInfoXml(Integer jokeType) {
        return jokeMapper.selectJokeInfoXml(jokeType);
    }
}
