package com.elife.mp.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.elife.mp.pojo.Joke;
import com.elife.mp.vo.JokeInfoVO;

import java.util.List;
import java.util.Map;

public interface JokeService {
    List<Joke> selectAllJoke();

    int insertJoke(Joke joke);
    int updateJokeById(Joke joke);
    int deleteById(Integer id);


    Joke selectById(Integer id);
    List<Joke> selectByMap(Map map);
    List<Joke> selectByIdList(List<Integer> ids);
    List<Joke> selectWrapper(Joke joke);
    IPage<Joke> selectByPage(Joke joke, int currPage, int pageSize);

    List<JokeInfoVO> selectJokeInfo(Integer jokeType);

    List<JokeInfoVO> selectJokeInfoXml(Integer jokeType);

}
