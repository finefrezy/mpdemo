package com.elife.mp.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class JokeInfoVO {
    @TableId(value="id",type = IdType.AUTO)
    private Long id;
    private String jokeName;
    private String jokeContent;
    private Integer jokeType;
    private Date createTime;
    private String typeName;
}
