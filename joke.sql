/*
 Navicat Premium Data Transfer

 Source Server         : home
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : dessert

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 05/08/2020 20:04:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for joke
-- ----------------------------
DROP TABLE IF EXISTS `joke`;
CREATE TABLE `joke`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `joke_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `joke_content` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `joke_type` smallint(5) UNSIGNED NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of joke
-- ----------------------------
INSERT INTO `joke` VALUES (1, '脱发', '看着身边的朋友，有的脱贫，有的脱单，还有更厉害的人，脱颖而出，说实话，我一点都不羡慕他们，我有的，他们未必有，因为我脱发', 1, '2020-07-16 15:26:14', NULL);
INSERT INTO `joke` VALUES (2, '象棋', '\r\n有人问我，在象棋里，帅有什么用处，到头来，还不是被小兵吃掉，\r\n在这里我想说，有士陪着，有马骑着\r\n有车开着，最重要的是还有像 暗恋者，帅有什么不好', 1, '2020-07-16 15:26:50', NULL);
INSERT INTO `joke` VALUES (3, '奋斗', '儿子：爸，你说你年轻的时候，要是多吃点苦受点累，我就不成富二代了吗\r\n哎，爸已经成这样了，所以你要多吃点苦受点累，让你的儿子成为富二代\\n\r\n爸:想得美，让我吃苦受累，让那兔带子享福 做梦去吧\r\n儿子，你说的对啊，爸当时就这么想的', 1, '2020-07-16 15:28:00', NULL);
INSERT INTO `joke` VALUES (4, '快递', '你学的生物，那我问你，什么生物有超强的臂力\r\n锋利的指甲，尖锐的牙齿，以至于能撕碎一切坚硬牢固的东西呢\r\n拆快递的女人', 1, '2020-07-16 15:28:18', NULL);
INSERT INTO `joke` VALUES (5, '皮筋', '我这有一块2块钱的皮筋，你用什么方法能让他快速增值\r\n把他绑在大闸蟹上卖', 1, '2020-07-16 15:28:33', NULL);
INSERT INTO `joke` VALUES (6, '热', '你用一句话形容你所在的城市有多热\r\n你说什么，冰箱里信号不好', 1, '2020-07-16 15:29:47', NULL);
INSERT INTO `joke` VALUES (7, '靠右行走', '你来应聘公司的司机，那我问你人和车为什么靠右停\r\n菩萨保佑', 3, '2020-07-16 15:32:32', NULL);
INSERT INTO `joke` VALUES (8, '螃蟹', '螃蟹为什么横着走呢\r\n有钳任性', 4, '2020-07-16 15:32:45', NULL);
INSERT INTO `joke` VALUES (9, '脑子进水', '面前有一个很深的坑，里面没有水，如果你跳进去，怎么出来\r\n把脑子里的水放出来，我就能飘起来了呀\r\n你脑子里有那么多水吗\r\n我脑子里每那么多水，我干嘛要跳下去呢', 2, '2020-07-16 15:33:08', NULL);
INSERT INTO `joke` VALUES (10, '大禹与悟空', '大禹治水，三过家门而不入，好像没发生什么事啊，其实是发生大事了\r\n因为他媳妇想盼着他回来，变成了望夫石，对吧\r\n孙悟空是打哪出来的，石头里蹦出来的，为什么大禹的定海神针孙悟空叫的好使\r\n因为那是他爸留给他的', 5, '2020-07-16 15:33:26', NULL);
INSERT INTO `joke` VALUES (11, '一块钱', '刚才我遇到以前的老板，说实话，我挺感谢他的，如果不是当初他解雇我，我也过不上\r\n现在舒适闲散，不劳而获的生活，老板页特别客气，大方的，往我碗里扔了一块钱', 1, '2020-07-16 15:33:44', NULL);
INSERT INTO `joke` VALUES (12, '追我啊', '现在很多女生，特别喜欢追星，而且还很痴迷，在这里我想说，毕竟明星跟我们的距离很远\r\n想要跟他们在一起，一点都不现实，有那个时间，你还不如追一下我，我很好追，一追就送给你\r\n', 6, '2020-07-16 15:34:10', NULL);
INSERT INTO `joke` VALUES (14, 'bbb', 'aaa', 1, '2020-08-05 15:08:19', NULL);

SET FOREIGN_KEY_CHECKS = 1;
