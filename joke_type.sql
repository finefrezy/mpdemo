/*
 Navicat Premium Data Transfer

 Source Server         : home
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : dessert

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 06/08/2020 13:24:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for joke_type
-- ----------------------------
DROP TABLE IF EXISTS `joke_type`;
CREATE TABLE `joke_type`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of joke_type
-- ----------------------------
INSERT INTO `joke_type` VALUES (1, '生活', '2020-07-11 16:51:01', NULL);
INSERT INTO `joke_type` VALUES (2, '学习', '2020-07-11 16:51:28', NULL);
INSERT INTO `joke_type` VALUES (3, '工作', '2020-07-11 16:51:48', NULL);
INSERT INTO `joke_type` VALUES (4, '急转弯', '2020-07-16 16:29:16', NULL);
INSERT INTO `joke_type` VALUES (5, '神话', '2020-07-16 16:30:30', NULL);
INSERT INTO `joke_type` VALUES (6, '恋爱', '2020-07-16 16:49:06', NULL);

SET FOREIGN_KEY_CHECKS = 1;
